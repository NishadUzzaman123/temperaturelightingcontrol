package serverclient;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * @author Nishad Uzzaman
 */
public class ControllerClient {
    //declaring class instance variables
    private DataOutputStream dout;
    private DataInputStream din;
    private Socket s;
    private boolean stop;
    private CommunicationInterface communicationInterface;

    /**
     * Class constructor starts a new Client and connects to the server
     *
     * @param host server host
     * @param port the client port
     */
    public ControllerClient(String host, int port, String controllerName, CommunicationInterface communicationInterface) {
        stop = false;
        this.communicationInterface = communicationInterface;
        try {
            s = new Socket(host, port);
            dout = new DataOutputStream(s.getOutputStream());
            din = new DataInputStream(s.getInputStream());

            //first message to server
            dout.writeUTF(controllerName);
            dout.flush();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    listenToServer();
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
            stop = true;
        }
    }


    /**
     * Keep listening to messages from the server
     */
    private void listenToServer() {
        try {
            String serverMessage;
            while (!stop) {
                //send to the server
//                dout.writeUTF(str);
//                dout.flush();
                serverMessage = din.readUTF();
                System.out.println("Server says: " + serverMessage);
                communicationInterface.messageReceivedByClient(serverMessage);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void closeClient() {
        try {
            stop = true;
            //send to the server stop
            dout.writeUTF("Stop");
            dout.flush();
//            String str2 = din.readUTF();
//            System.out.println("Server says: " + str2);
            //close the socket
            dout.close();
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
            stop = true;
        }
    }

    /**
     * Check whether it is temperature controller
     * @return True if temperature controller else false
     */
    public static boolean isTemperatureController(String controllerName) {
        return controllerName.equals("temperatureController");
    }

    /**
     * Check whether it is light controller
     * @return True if light controller else false
     */
    public static boolean isLightController(String controllerName) {
        return controllerName.equals("lightController");
    }
}
