package serverclient;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Nishad Uzzaman
 */
public class Server {
    //declare class instance variables
    private static int delay;
    private static String fileName;
    private static final List<String> controllerNames = new ArrayList<>();
    private static List<DataOutputStream> dataOutputStreams;

    /**
     * Class constructor which creates a new Server object and starts the server
     *
     * @param portNumber server socket port
     * @param delay      the delay time in seconds
     * @param fileName   the name containing temperature and light values
     */
    public Server(int portNumber, int delay, String fileName) {
        this.delay = delay;
        this.fileName = fileName;
        dataOutputStreams = new ArrayList<>();
        startServer(portNumber);
    }

    /**
     * Start the server and start accepting clients
     * @param port server socket port
     */
    private void startServer(int port) {
        ServerSocket ss;
        Socket s;
        try {
            ss = new ServerSocket(port);
            System.out.println("The server is listening to port " + port);
            System.out.println("Server is waiting for clients to connection");
            while (true) {
                s = ss.accept();
                System.out.println("A new client has connected " + s);
                new Thread(new Handler(s)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * The main method
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 3) {
            //
            System.out.println("Make sure you have provided the port number, time delay, and the name of the data file");
        } else {
            //get port number, time delay and name of the file from the command arguments
            int portNumber = Integer.parseInt(args[0]);
            int delay = Integer.parseInt(args[1]);
            String fileName = args[2];
            //create an object of the server
            Server server = new Server(portNumber, delay, fileName);
        }
    }

    /**
     * Read the contents of the data file
     * @param out DataOutputStream of the client
     */
    private static void readFileContents(DataOutputStream out, String controllerName) {
        boolean continueReading = true;
        try {
            //start reading data (rows) from the beginning after reading all the lines
            while (true) {
                Scanner fileReader = new Scanner(new File(fileName));
                //remove the headers of the csv file
                fileReader.nextLine();
                //read all rows from file
                while (fileReader.hasNextLine()) {
                    //read the current row values
                    String currentLine = fileReader.nextLine();
                    String[] rowValues = currentLine.split(",");
                    //read temperature
                    double temperature = Double.parseDouble(rowValues[9]);
                    //read light value
                    double light = Double.parseDouble(rowValues[7]);
                    //pass message to the client
                    if (ControllerClient.isTemperatureController(controllerName)) {
                        out.writeUTF(controllerName + "-" + temperature);
                    } else if (ControllerClient.isLightController(controllerName)) {
                        out.writeUTF(controllerName + "-" + light);
                    }
                    out.flush();
                    // each row should be read with a delay of n seconds
                    //delay for delay value seconds
                    try{
                        Thread.sleep(delay * 1000);
                    } catch(InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * The client handler task.
     */
    private static class Handler implements Runnable {
        private String controllerName;
        private Socket socket;
        private DataInputStream in;
        private DataOutputStream out;

        /**
         * Class constructor which creates a handler thread
         * @param socket Client socket
         */
        public Handler(Socket socket) {
            this.socket = socket;
        }

        /**
         * continuously serve the client until the client sends Stop instructions
         */
        @Override
        public void run() {
            try {
                in = new DataInputStream(socket.getInputStream());
                out = new DataOutputStream(socket.getOutputStream());

                // wait for the client to send its name
                while (true) {
                    controllerName = in.readUTF();
                    System.out.println("Hello there: " + controllerName);
                    if (controllerName == null) {
                        return;
                    }
                    //add the controller name to the list of controller names
                    synchronized (controllerNames) {
                        if (!controllerName.trim().equalsIgnoreCase("") && !controllerNames.contains(controllerName)) {
                            controllerNames.add(controllerName);
                            break;
                        }
                    }
                }
                dataOutputStreams.add(out);
                out.writeUTF("Welcome " + controllerName);
                out.flush();

                //another thread to read contents from the file
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //start reading records from the file
                        readFileContents(out, controllerName);
                    }
                }).start();

                //Accept messages from this client.
                while (true) {
                    String input = in.readUTF();
                    System.out.println("Stopping");
                    if (input.equals("Stop")) {//read stop message
                        return;
                    }
                    out.writeUTF("Received the message: " + input);
                    out.flush();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (out != null) {
                    dataOutputStreams.remove(out);
                }
                if (controllerName != null) {
                    System.out.println(controllerName + " is now stopping");
                    controllerNames.remove(controllerName);
                }
                try {
                    socket.close();
                    if (dataOutputStreams.size() == 0) {

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
