package serverclient;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This class creates a JPanel with a rotating fan
 * @author Nishad Uzzaman
 */
public class FanPanel extends JPanel {
    int fanSpeed;
    double currentTheta;
    Timer timer;
    public FanPanel() {
        fanSpeed = 1000;
        timer = new Timer(fanSpeed, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //rotate fan
                rotateTheFan();
            }
        });
        timer.setInitialDelay(100);
        timer.start();
    }

    /**
     * This method creates a new rotation angle
     */
    public void moveTheta() {
        double newTheta = currentTheta + 1.0;
        if (newTheta >= 2 * Math.PI) {
            newTheta -= 2 * Math.PI;
        }
        currentTheta = newTheta;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //get the new angle in radians
        moveTheta();
        int xCenter = 0;
        int yCenter = 0;
        //find the radius of the fan
        int radius = (int) (Math.min(getWidth(), getHeight()) * 0.4);
        int x = xCenter - radius;
        int y = yCenter - radius;
        g2d.translate(this.getWidth() / 2, this.getHeight() / 2);
        //rotate the fan and place it at currentTheta from its original position
        g2d.rotate(currentTheta);
        //draw the arcs fan
        g.setColor(Color.CYAN);
        g.fillArc(x, y, 2 * radius, 2 * radius, 0, 45);
        g.fillArc(x, y, 2 * radius, 2 * radius, 90, 45);
        g.fillArc(x, y, 2 * radius, 2 * radius, 180, 45);
        g.fillArc(x, y, 2 * radius, 2 * radius, 270, 45);
    }

    private void rotateTheFan() {
        repaint();
    }

    /**
     * This method adjust the speed of the fan if the value passed is greater than 5
     * @param newSpeed new fan speed
     */
    public void adjustSpeed(int newSpeed) {
        System.out.println("New fan speed:" + newSpeed);
        if (newSpeed > 5) {
            this.fanSpeed = newSpeed;
            timer.setDelay(fanSpeed);
            timer.restart();
        }
    }
}
