package serverclient;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

/**
 * @author Nishad Uzzaman
 */
public class TemperatureLightingControl extends JFrame implements CommunicationInterface {
    private JPanel contentPane;
    private FanPanel fanPanel;

    /**
     * Class constructor
     * @param host server host
     * @param port port where the clients will run from
     */
    public TemperatureLightingControl(String host, int port) {
        setTitle("Room temperature and light control");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 626, 400);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);


        fanPanel = new FanPanel();
        fanPanel.setBackground(Color.getHSBColor(210, 75, 156));
        fanPanel.setPreferredSize( new Dimension( 400, 300 ));
        contentPane.add(fanPanel, BorderLayout.WEST);

        JButton btnNewButton = new JButton("STOP");
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

        rightPanel.add(btnNewButton);
        contentPane.add(rightPanel, BorderLayout.EAST);

        ControllerClient temperatureController = new ControllerClient(host, port, "temperatureController", this);
        ControllerClient lightController = new ControllerClient(host, port, "lightController", this);

        btnNewButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                temperatureController.closeClient();
                lightController.closeClient();
            }
        });
    }

    /**
     * The main method
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 2) {
            //
            System.out.println("Passed the host and the port number");
        } else {
            String host = args[0];
            int port = Integer.parseInt(args[1]);
            EventQueue.invokeLater(new Runnable() {
                public void run() {
                    try {
                        TemperatureLightingControl frame = new TemperatureLightingControl(host, port);
                        frame.setVisible(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    public void messageReceivedByClient(String message) {
        String[] messageContents = message.split("-");
        //ensure that the message was sent by server in the right format
        //message should have both controller name and value representing either temperature or light
        if (messageContents.length == 2) {
            if (ControllerClient.isTemperatureController(messageContents[0])) {//check whether the message was sent to temperature controller
                fanPanel.adjustSpeed((int) Double.parseDouble(messageContents[1]));
            } else if (ControllerClient.isLightController(messageContents[0])) {
                //change the brightness of the color
                double light = Double.parseDouble(messageContents[1]);
                Color currentBackground = fanPanel.getBackground();
                fanPanel.setBackground(Color.getHSBColor((float) light, currentBackground.getRed(), currentBackground.getBlue()));
            }
        }
    }
}
