package serverclient;
/**
 * This interface will be used to pass message from the client to the main application
 * @author Nishad Uzzaman
 */
public interface CommunicationInterface {
    void messageReceivedByClient(String message);
}
